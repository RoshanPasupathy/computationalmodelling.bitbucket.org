<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <title>Computational Modelling Tools Workshops - latex-with-lyx</title>
        <link rel="stylesheet" href="./theme/css/main.css" />

        <!-- Addition to include keys -->
        <link rel="stylesheet" href="./theme/css/keys.css" type="text/css" />

        <!--[if IE]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
</head>

<body id="index" class="home">
        <header id="banner" class="body">
                <h1><a href="./">Computational Modelling Tools Workshops </a></h1>
                <nav><ul>
                    <li><a href="about.html">About</a></li>
                    <li><a href="archives.html">Archive</a></li>
                    <li><a href="tags.html">Tags</a></li>
                </ul></nav>
        </header><!-- /#banner -->

            <aside id="featured" class="body">
                <article>
                    <h1 class="entry-title"><a href="./introduction-to-latex-using-lyx.html">Introduction to LaTeX using LyX</a></h1>
<footer class="post-info">
        <abbr class="published" title="2017-03-16T00:00:00+01:00">
                Published: Thu 16 March 2017
        </abbr>

        <address class="vcard author">
                By                         <a class="url fn" href="./author-jack-saywell.html">Jack Saywell</a>
                        <a class="url fn" href="./author-juraj-mihalik.html">Juraj Mihalik</a>
        </address>
<p>In <a href="./category-latex-with-lyx.html">latex-with-lyx</a>. </p>
<p>tags: <a href="./tag-lyx.html">Lyx</a> <a href="./tag-latex.html">Latex</a> <a href="./tag-document-creation.html">document creation</a> </p>
</footer><!-- /.post-info --><p><img alt="LyX" src="./latex-with-lyx/lyx.png" title="LyX"></p>
<h1>Introduction</h1>
<p>This workshop serves as an introduction to LaTeX, using the graphical user interface LyX. We explain the advantages of using LyX, and provide an exercise with the aim of producing a basic template of a scientific report.</p>
<p>Our motivation is to allow people to quickly gain familiarity with LaTeX.</p>
<p>LaTeX is the way to write academic literature in the sciences. This means it’s essential to learn at some point. Previous workshops have introduced the topic in a theoretical and rigorous way, which doesn’t lend itself to people who want to quickly gain familiarity, and so for this reason we are adopting a slightly different approach.</p>
<p>It is often remarked that LaTeX is the antithesis to the mantra WYSIWYG (What You See Is What You Get), and this is very true. LaTeX is essentially code (a .tek file), which is compiled to form a document (e.g. pdf). The beauty of LaTeX comes from its customisability and how it can represent completely general mathematical equations in a beautiful and consistent way.</p>
<p>We are going to introduce LyX, a piece of software which is based on the philosophy WYSIWYM (What You See Is What You Mean) and is more akin to a word processing suite e.g. MS Word than writing bare LaTeX. However, it encourages a structured approach to document creation and allows combination of flexibility of LaTeX with the ease of a word processor. Its main advantage is that you see how the document appears in a clear way as you write it, and don't need to repeatedly re-compile into a pdf. This makes the document writing process more fluid.</p>
<p>Why LaTeX?</p>
<ul>
<li>Equations</li>
<li>Figures</li>
<li>Consistent formatting</li>
<li>Cross referencing</li>
<li>Bibliography</li>
</ul>
<p>Why LyX?</p>
<ul>
<li>Open source GUI</li>
<li>Cross-platform</li>
<li>Multiple online resources</li>
<li>Ease of formatting</li>
<li>Version control</li>
<li>Spellchecker</li>
<li>Export to LaTeX</li>
</ul>
<h1>LyX</h1>
<p>To get LyX, simply download from their website or use the virtual machine provided.</p>
<ul>
<li><a href="https://www.lyx.org/">LyX</a></li>
</ul>
<p>Both LyX and LaTeX require a TeX distribution is installed on your machine. Popular options include TeXLive or MiKTeX:</p>
<ul>
<li><a href="https://www.tug.org/texlive/">TeXLive</a></li>
<li><a href="https://miktex.org/">MiKTeX</a></li>
</ul>
<p>The beauty of LyX lies in its ease of use. Instead of writing LaTeX commands to insert tables/titles/sections/etc, one simply selects from drop down menus. LyX also has a user-friendly equation editor.</p>
<p>After you've played around with LyX and written a document, you can view the resulting pdf by clicking on the 'eyes' icon in the toolbar. If you make further changes to your document, clicking the 'cyclic arrows' icon button just to the right of the 'eyes' icon updates the pdf with any changes.</p>
<p>The best way to discover the features of LyX is by experimentation, and for those in search of a more complete tutorial more details can be found <a href="http://wiki.lyx.org/LyX/Tutorials">here</a>.</p>
<h1>LaTeX</h1>
<p>We now present a brief introduction on how to construct a document using LaTeX. For a more thorough introduction to LaTeX, we refer to an excellent previous workshop which can be found <a href="https://computationalmodelling.bitbucket.io/tools/introduction-to-latex.html">here</a>.</p>
<p>To get started with LaTex you will need a text editor/IDE (eg. Emacs, Sublime or TeXMaker) and a PDF viewer (eg. Evince, Adobe) in addition to a TeX distribution.</p>
<h2>Document structure</h2>
<p>Begin by using a preferred text editor, or LaTeX IDE. A LaTeX document is formed around commands:</p>
<div class="highlight"><pre><span></span><span class="k">\command</span><span class="na">[option]</span><span class="nb">{</span>argument<span class="nb">}</span>
</pre></div>


<p>The document should begin by defining the document class. Different classes have different purposes and properties. The most common for scientific documents will be the article class. Note the options provided to the command.</p>
<div class="highlight"><pre><span></span><span class="k">\documentclass</span><span class="na">[a4paper,12pt]</span><span class="nb">{</span>article<span class="nb">}</span>
</pre></div>


<p>Packages can be added to extend basic functionality,</p>
<div class="highlight"><pre><span></span><span class="k">\usepackage</span><span class="na">[opt1, opt2, ...]</span><span class="nb">{</span>package<span class="nb">_</span>name<span class="nb">}</span>
</pre></div>


<p>Two useful such packages are <em>amsmath</em> and <em>graphicx</em>, which allow for inclusion of a wide range of mathematical expressions and the importing of figures, respectively.</p>
<p>The body of the document must be contained between the following:</p>
<div class="highlight"><pre><span></span><span class="k">\begin</span><span class="nb">{</span>document<span class="nb">}</span>

<span class="k">\end</span><span class="nb">{</span>document<span class="nb">}</span>
</pre></div>


<h2>Creating a title</h2>
<p>To specify the title, author and date, use the following commands at the beginning of the document:</p>
<div class="highlight"><pre><span></span><span class="k">\begin</span><span class="nb">{</span>document<span class="nb">}</span>
<span class="k">\title</span><span class="nb">{</span>Proof of Fermat’s Last Theorem<span class="nb">}</span>
<span class="k">\author</span><span class="nb">{</span>Andrew Wiles<span class="nb">}</span>
<span class="k">\date</span><span class="nb">{</span>December 1994<span class="nb">}</span>
<span class="k">\maketitle</span>
<span class="k">\end</span><span class="nb">{</span>document<span class="nb">}</span>
</pre></div>


<h2>Sections</h2>
<p>Just like LyX we have sections and subsections. Unlike LyX, we define them using commands. For example,</p>
<div class="highlight"><pre><span></span><span class="k">\section</span><span class="nb">{</span>Calculus<span class="nb">}</span>
<span class="k">\subsection</span><span class="nb">{</span>Differentiation<span class="nb">}</span>
<span class="k">\section*</span><span class="nb">{</span>Integration<span class="nb">}</span>        (this will be unnumbered)
<span class="k">\section</span><span class="nb">{</span>Calculus<span class="nb">}</span><span class="k">\label</span><span class="nb">{</span>sec:Calculus<span class="nb">}</span>
</pre></div>


<p>We can then refer to the <em>'Calculus'</em> section by using the following command within text:</p>
<div class="highlight"><pre><span></span><span class="k">\ref</span><span class="nb">{</span>sec:Calculus<span class="nb">}</span>
</pre></div>


<h2>Equations and Figures</h2>
<p>At the most basic level, equations can be included inline or separately to allow for numbering. To include inline simply include the following within text:</p>
<div class="highlight"><pre><span></span><span class="s">$</span><span class="nb">equation</span><span class="s">$</span>
</pre></div>


<p>The alternative is to use the equation environment:</p>
<div class="highlight"><pre><span></span><span class="k">\begin</span><span class="nb">{</span>equation<span class="nb">}</span>
  y = x<span class="nb">_</span>0 + <span class="k">\frac</span><span class="nb">{</span>1<span class="nb">}{</span><span class="k">\displaystyle</span> x<span class="nb">_</span>1
          + <span class="k">\frac</span><span class="nb">{</span>1<span class="nb">}{</span><span class="k">\displaystyle</span> x<span class="nb">_</span>2
          + <span class="k">\frac</span><span class="nb">{</span>1<span class="nb">}{</span><span class="k">\displaystyle</span> x<span class="nb">_</span>3 + x<span class="nb">_</span>4<span class="nb">}}}</span> <span class="k">\nonumber</span>
<span class="k">\end</span><span class="nb">{</span>equation<span class="nb">}</span>
</pre></div>


<p>To include figures, the basic code structure is:</p>
<div class="highlight"><pre><span></span><span class="k">\begin</span><span class="nb">{</span>figure<span class="nb">}</span>
<span class="k">\centering</span>
  <span class="k">\includegraphics</span><span class="na">[scale=0.8]</span><span class="nb">{</span>image.png<span class="nb">}</span>
  <span class="k">\caption</span><span class="nb">{</span>A caption.<span class="nb">}</span>
  <span class="k">\label</span><span class="nb">{</span>fig:image<span class="nb">}</span>
<span class="k">\end</span><span class="nb">{</span>figure<span class="nb">}</span>
</pre></div>


<p>Note that we have centered the image and added a suitable caption and label, which can be referred to within the text.</p>
<h2>Bibliography</h2>
<p>If you have a small number of references you can use LaTeX's bibliography environment as follows:</p>
<div class="highlight"><pre><span></span><span class="k">\begin</span><span class="nb">{</span>thebibliography<span class="nb">}{</span>99<span class="nb">}</span>

<span class="k">\bibitem</span><span class="nb">{</span>ref1<span class="nb">}</span> Person A, Person A&#39;s Paper,
Journal A (Date A).

<span class="k">\bibitem</span><span class="nb">{</span>ref2<span class="nb">}</span> Person B,
Person B&#39;s Book, Publisher B (Date B).

<span class="k">\end</span><span class="nb">{</span>thebibliography<span class="nb">}</span>
</pre></div>


<p>Items are added using the '\bibitem{label}' command. You can use the '\cite{}' command in text to refer to particular entries in the bibliography.</p>
<p>For instructions on how to use BibTeX for referencing, we refer to the excellent previous workshop by Gabriele Boschetto and Alejandra Vergara, which can be found <a href="https://computationalmodelling.bitbucket.io/tools/Bibtex.html">here</a>.</p>
<h1>References and useful links</h1>
<ul>
<li><a href="https://www.lyx.org/">LyX</a></li>
<li><a href="https://computationalmodelling.bitbucket.io/tools/Bibtex.html">Introduction to LaTeX and BibTeX</a></li>
<li><a href="https://computationalmodelling.bitbucket.io/tools/introduction-to-latex.html">Introduction to LaTeX</a></li>
<li><a href="http://en.wikibooks.org/wiki/LaTeX">Latex Wiki</a></li>
<li><a href="https://tobi.oetiker.ch/lshort/lshort.pdf">The Not So Short Introduction to Latex</a></li>
</ul>
<h1>Exercise</h1>
<p>Try to recreate the following pdf as close as you can, you can use either LyX or, if you are more comfortable, LaTeX to do this. Feel free to be creative, there's no need to laboriously recreate the exercise pdf exactly, as long as you can demonstrate the main features.</p>
<ul>
<li>Exercise: <a href="./latex-with-lyx/Exercise_latex.pdf">pdf</a></li>
<li>Cheat sheet: <a href="./latex-with-lyx/cheat_sheet_2.pdf">pdf</a></li>
</ul>
<p>The aim here is to both show that LyX is far quicker to reproduce the basic document structure needed for scientific reports and also allow people to gain familiarity with LaTeX.</p>
<ul>
<li>Solutions in LyX: <a href="./latex-with-lyx/Exercise_lyx.lyx">lyx</a></li>
<li>Solutions in LaTeK: <a href="./latex-with-lyx/Exercise_latex.tex">tex</a></li>
<li>.bib file for bibliography: <a href="./latex-with-lyx/Exercise_bibliography.bib">bib</a></li>
<li>Image for exercise: <a href="./latex-with-lyx/file_extensions.png">png</a></li>
</ul>                </article>
            </aside><!-- /#featured -->
        <section id="extras" class="body">
                <div class="blogroll">
                        <h2>blogroll</h2>
                        <ul>
                            <li><a href="http://www.ngcm.soton.ac.uk/blog/">NGCM Blog</a></li>
                            <li><a href="http://cmg.soton.ac.uk/">CMG</a></li>
                            <li><a href="http://www.southampton.ac.uk">Uni Southampton</a></li>
                        </ul>
                </div><!-- /.blogroll -->
                <div class="social">
                        <h2>social</h2>
                        <ul>

                            <li><a href="https://twitter.com/NGCM_Soton">NGCM Twitter</a></li>
                        </ul>
                </div><!-- /.social -->
        </section><!-- /#extras -->
        
        <!--         
        <footer id="contentinfo" class="body">
                <address id="about" class="vcard body">
                Proudly powered by <a href="http://getpelican.com/">Pelican</a>, which takes great advantage of <a href="http://python.org">Python</a>.
        -->

                </address><!-- /#about -->
                <!--
                <p>The theme is by <a href="http://coding.smashingmagazine.com/2009/08/04/designing-a-html-5-layout-from-scratch/">Smashing Magazine</a>, thanks!</p>
            -->
        </footer><!-- /#contentinfo -->

</body>
</html>